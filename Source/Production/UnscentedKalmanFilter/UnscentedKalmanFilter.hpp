/**
 ******************************************************************************
 * @file    UnscentedKalmanFilter.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    09 de jan de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_UNSCENTED_KALMAN_FILTER_UNSCENTED_KALMAN_FILTER_HPP
#define SOURCE_PRODUCTION_UNSCENTED_KALMAN_FILTER_UNSCENTED_KALMAN_FILTER_HPP

namespace Filters
{
   class UnscentedKalmanFilter
   {
      private:

      protected:

      public:
         /**
          * @brief
          */
         UnscentedKalmanFilter();

         /**
          * @brief
          */
         virtual ~UnscentedKalmanFilter();
   };
}

#endif // SOURCE_PRODUCTION_UNSCENTED_KALMAN_FILTER_UNSCENTED_KALMAN_FILTER_HPP
