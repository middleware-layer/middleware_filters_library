/**
 ******************************************************************************
 * @file    UnscentedKalmanFilter.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    09 de jan de 2018
 * @brief
 ******************************************************************************
 */

#include <Production/UnscentedKalmanFilter/UnscentedKalmanFilter.hpp>

namespace Filters
{
   UnscentedKalmanFilter::UnscentedKalmanFilter()
   {
   }

   UnscentedKalmanFilter::~UnscentedKalmanFilter()
   {
   }
}
