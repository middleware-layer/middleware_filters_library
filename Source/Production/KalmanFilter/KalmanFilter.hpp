/**
 ******************************************************************************
 * @file    KalmanFilter.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    09 de jan de 2018
 * @brief   A really nice and well-explained website about Kalman Filters
 *          http://www.bzarg.com/p/how-a-kalman-filter-works-in-pictures/
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_KALMAN_FILTER_KALMAN_FILTER_HPP
#define SOURCE_PRODUCTION_KALMAN_FILTER_KALMAN_FILTER_HPP

namespace Filters
{
   class KalmanFilter
   {
      private:

      protected:

      public:
         /**
          * @brief
          */
         KalmanFilter();

         /**
          * @brief
          */
         virtual ~KalmanFilter();
   };
}

#endif // SOURCE_PRODUCTION_KALMAN_FILTER_KALMAN_FILTER_HPP
