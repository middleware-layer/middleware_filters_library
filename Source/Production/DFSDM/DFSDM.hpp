/**
 ******************************************************************************
 * @file    DFSDM.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    16 de mar de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_DFSDM_DFSDM_HPP
#define SOURCE_PRODUCTION_DFSDM_DFSDM_HPP

namespace Filters
{
   class DFSDM
   {
      private:

      protected:

      public:
         /**
          * @brief
          */
         DFSDM();

         /**
          * @brief
          */
         virtual ~DFSDM();
   };
}

#endif // SOURCE_PRODUCTION_DFSDM_DFSDM_HPP
