/**
 ******************************************************************************
 * @file    ExtendedKalmanFilter.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    09 de jan de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_EXTENDED_KALMAN_FILTER_EXTENDED_KALMAN_FILTER_HPP
#define SOURCE_PRODUCTION_EXTENDED_KALMAN_FILTER_EXTENDED_KALMAN_FILTER_HPP

namespace Filters
{
   class ExtendedKalmanFilter
   {
      private:

      protected:

      public:
         /**
          * @brief
          */
         ExtendedKalmanFilter();

         /**
          * @brief
          */
         virtual ~ExtendedKalmanFilter();
   };
}

#endif // SOURCE_PRODUCTION_EXTENDED_KALMAN_FILTER_EXTENDED_KALMAN_FILTER_HPP
