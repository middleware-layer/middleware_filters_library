/**
 ******************************************************************************
 * @file    ExtendedKalmanFilter.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    09 de jan de 2018
 * @brief
 ******************************************************************************
 */

#include <Production/ExtendedKalmanFilter/ExtendedKalmanFilter.hpp>

namespace Filters
{
   ExtendedKalmanFilter::ExtendedKalmanFilter()
   {
   }

   ExtendedKalmanFilter::~ExtendedKalmanFilter()
   {
   }
}
