/**
 ******************************************************************************
 * @file    NMEA_test.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2018
 * @brief
 ******************************************************************************
 */

#include <gtest/gtest.h>

#include <Production/UnscentedKalmanFilter/UnscentedKalmanFilter.hpp>

#include <string>

namespace Filters
{
   class UnscentedKalmanFilterTest : public ::testing::Test
   {
      private:

      protected:
         UnscentedKalmanFilter m_UnscentedKalmanFilter;

      public:
         /**
          * @brief	This method will run always before each test
          */
         void SetUp(void)
         {

         }

         /**
          * @brief	This method will run always after each test
          */
         void TearDown(void)
         {

         }
   };

   TEST_F(UnscentedKalmanFilterTest, ConstructorCalled_Success)
   {
   }
}
