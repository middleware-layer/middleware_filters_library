/**
 ******************************************************************************
 * @file    KalmanFilter.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    16 de mar de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUB_KALMAN_FILTER_KALMAN_FILTER_HPP
#define SOURCE_TESTING_STUB_KALMAN_FILTER_KALMAN_FILTER_HPP

#include <Production/KalmanFilter/KalmanFilter.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Filters
{
   class KalmanFilter_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_KalmanFilter
   {
      public:
         virtual ~I_KalmanFilter();
   };

   class _Mock_KalmanFilter : public I_KalmanFilter
   {
      public:
         _Mock_KalmanFilter();
   };

   typedef ::testing::NiceMock<_Mock_KalmanFilter> Mock_KalmanFilter;

   void stub_setImpl(I_KalmanFilter* pNewImpl);
}

#endif // SOURCE_TESTING_STUB_KALMAN_FILTER_KALMAN_FILTER_HPP
