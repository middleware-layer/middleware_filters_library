/**
 ******************************************************************************
 * @file    KalmanFilter_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/KalmanFilter/KalmanFilter_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Filters
{
   static I_KalmanFilter* p_KalmanFilter_Impl = NULL;

   const char* KalmanFilter_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle KalmanFilter functions";
   }

   void stub_setImpl(I_KalmanFilter* pNewImpl)
   {
      p_KalmanFilter_Impl = pNewImpl;
   }

   static I_KalmanFilter* KalmanFilter_Stub_Get_Impl(void)
   {
      if(p_KalmanFilter_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to KalmanFilter functions. Did you forget"
                   << "to call Stubs::Filters::stub_setImpl() ?" << std::endl;

         throw KalmanFilter_StubImplNotSetException();
      }

      return p_KalmanFilter_Impl;
   }

      ///////////////////////////////////////
      // I_KalmanFilter Methods Definition //
      ///////////////////////////////////////

   I_KalmanFilter::~I_KalmanFilter()
   {
      if(p_KalmanFilter_Impl == this)
      {
         p_KalmanFilter_Impl = NULL;
      }
   }

   ///////////////////////////////////////////
   // _Mock_KalmanFilter Methods Definition //
   ///////////////////////////////////////////

   _Mock_KalmanFilter::_Mock_KalmanFilter()
   {
   }

           //////////////////////////////////
           // Definition of Non Stub Class //
           //////////////////////////////////

   KalmanFilter::KalmanFilter()
   {
   }

   KalmanFilter::~KalmanFilter()
   {
   }
}

