/**
 ******************************************************************************
 * @file    DFSDM.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    16 de mar de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUB_DFSDM_DFSDM_HPP
#define SOURCE_TESTING_STUB_DFSDM_DFSDM_HPP

#include <Production/DFSDM/DFSDM.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Filters
{
   class DFSDM_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_DFSDM
   {
      public:
         virtual ~I_DFSDM();
   };

   class _Mock_DFSDM : public I_DFSDM
   {
      public:
         _Mock_DFSDM();
   };

   typedef ::testing::NiceMock<_Mock_DFSDM> Mock_DFSDM;

   void stub_setImpl(I_DFSDM* pNewImpl);
}

#endif // SOURCE_TESTING_STUB_DFSDM_DFSDM_HPP
