/**
 ******************************************************************************
 * @file    DFSDM_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/DFSDM/DFSDM_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Filters
{
   static I_DFSDM* p_DFSDM_Impl = NULL;

   const char* DFSDM_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle DFSDM functions";
   }

   void stub_setImpl(I_DFSDM* pNewImpl)
   {
      p_DFSDM_Impl = pNewImpl;
   }

   static I_DFSDM* DFSDM_Stub_Get_Impl(void)
   {
      if(p_DFSDM_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to DFSDM functions. Did you forget"
                   << "to call Stubs::Filters::stub_setImpl() ?" << std::endl;

         throw DFSDM_StubImplNotSetException();
      }

      return p_DFSDM_Impl;
   }

      ///////////////////////////////
      // I_DFSDM Methods Definition //
      ///////////////////////////////

   I_DFSDM::~I_DFSDM()
   {
      if(p_DFSDM_Impl == this)
      {
         p_DFSDM_Impl = NULL;
      }
   }

   ////////////////////////////////////
   // _Mock_DFSDM Methods Definition //
   ////////////////////////////////////

   _Mock_DFSDM::_Mock_DFSDM()
   {
   }

           //////////////////////////////////
           // Definition of Non Stub Class //
           //////////////////////////////////

   DFSDM::DFSDM()
   {
   }

   DFSDM::~DFSDM()
   {
   }
}

