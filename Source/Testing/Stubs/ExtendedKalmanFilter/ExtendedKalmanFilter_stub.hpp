/**
 ******************************************************************************
 * @file    ExtendedKalmanFilter.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    16 de mar de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUB_EXTENDED_KALMAN_FILTER_EXTENDED_KALMAN_FILTER_HPP
#define SOURCE_TESTING_STUB_EXTENDED_KALMAN_FILTER_EXTENDED_KALMAN_FILTER_HPP

#include <Production/ExtendedKalmanFilter/ExtendedKalmanFilter.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Filters
{
   class ExtendedKalmanFilter_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_ExtendedKalmanFilter
   {
      public:
         virtual ~I_ExtendedKalmanFilter();
   };

   class _Mock_ExtendedKalmanFilter : public I_ExtendedKalmanFilter
   {
      public:
         _Mock_ExtendedKalmanFilter();
   };

   typedef ::testing::NiceMock<_Mock_ExtendedKalmanFilter> Mock_ExtendedKalmanFilter;

   void stub_setImpl(I_ExtendedKalmanFilter* pNewImpl);
}

#endif // SOURCE_TESTING_STUB_EXTENDED_KALMAN_FILTER_EXTENDED_KALMAN_FILTER_HPP
