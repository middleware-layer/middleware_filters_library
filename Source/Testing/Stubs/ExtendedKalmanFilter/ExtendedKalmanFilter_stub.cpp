/**
 ******************************************************************************
 * @file    ExtendedKalmanFilter_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/ExtendedKalmanFilter/ExtendedKalmanFilter_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Filters
{
   static I_ExtendedKalmanFilter* p_ExtendedKalmanFilter_Impl = NULL;

   const char* ExtendedKalmanFilter_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle ExtendedKalmanFilter functions";
   }

   void stub_setImpl(I_ExtendedKalmanFilter* pNewImpl)
   {
      p_ExtendedKalmanFilter_Impl = pNewImpl;
   }

   static I_ExtendedKalmanFilter* ExtendedKalmanFilter_Stub_Get_Impl(void)
   {
      if(p_ExtendedKalmanFilter_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to ExtendedKalmanFilter functions. Did you forget"
                   << "to call Stubs::Filters::stub_setImpl() ?" << std::endl;

         throw ExtendedKalmanFilter_StubImplNotSetException();
      }

      return p_ExtendedKalmanFilter_Impl;
   }

      ///////////////////////////////////////////////
      // I_ExtendedKalmanFilter Methods Definition //
      ///////////////////////////////////////////////

   I_ExtendedKalmanFilter::~I_ExtendedKalmanFilter()
   {
      if(p_ExtendedKalmanFilter_Impl == this)
      {
         p_ExtendedKalmanFilter_Impl = NULL;
      }
   }

   ///////////////////////////////////////////////////
   // _Mock_ExtendedKalmanFilter Methods Definition //
   ///////////////////////////////////////////////////

   _Mock_ExtendedKalmanFilter::_Mock_ExtendedKalmanFilter()
   {
   }

           //////////////////////////////////
           // Definition of Non Stub Class //
           //////////////////////////////////

   ExtendedKalmanFilter::ExtendedKalmanFilter()
   {
   }

   ExtendedKalmanFilter::~ExtendedKalmanFilter()
   {
   }
}

