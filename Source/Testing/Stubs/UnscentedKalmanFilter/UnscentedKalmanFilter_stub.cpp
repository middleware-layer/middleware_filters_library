/**
 ******************************************************************************
 * @file    UnscentedKalmanFilter_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/UnscentedKalmanFilter/UnscentedKalmanFilter_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Filters
{
   static I_UnscentedKalmanFilter* p_UnscentedKalmanFilter_Impl = NULL;

   const char* UnscentedKalmanFilter_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle UnscentedKalmanFilter functions";
   }

   void stub_setImpl(I_UnscentedKalmanFilter* pNewImpl)
   {
      p_UnscentedKalmanFilter_Impl = pNewImpl;
   }

   static I_UnscentedKalmanFilter* UnscentedKalmanFilter_Stub_Get_Impl(void)
   {
      if(p_UnscentedKalmanFilter_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to UnscentedKalmanFilter functions. Did you forget"
                   << "to call Stubs::Filters::stub_setImpl() ?" << std::endl;

         throw UnscentedKalmanFilter_StubImplNotSetException();
      }

      return p_UnscentedKalmanFilter_Impl;
   }

      ////////////////////////////////////////////////
      // I_UnscentedKalmanFilter Methods Definition //
      ////////////////////////////////////////////////

   I_UnscentedKalmanFilter::~I_UnscentedKalmanFilter()
   {
      if(p_UnscentedKalmanFilter_Impl == this)
      {
         p_UnscentedKalmanFilter_Impl = NULL;
      }
   }

   ////////////////////////////////////////////////////
   // _Mock_UnscentedKalmanFilter Methods Definition //
   ////////////////////////////////////////////////////

   _Mock_UnscentedKalmanFilter::_Mock_UnscentedKalmanFilter()
   {
   }

           //////////////////////////////////
           // Definition of Non Stub Class //
           //////////////////////////////////

   UnscentedKalmanFilter::UnscentedKalmanFilter()
   {
   }

   UnscentedKalmanFilter::~UnscentedKalmanFilter()
   {
   }
}

