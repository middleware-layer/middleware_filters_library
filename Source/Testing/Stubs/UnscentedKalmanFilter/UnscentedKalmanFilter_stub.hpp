/**
 ******************************************************************************
 * @file    UnscentedKalmanFilter.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    16 de mar de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUB_UNSCENTED_KALMAN_FILTER_UNSCENTED_KALMAN_FILTER_HPP
#define SOURCE_TESTING_STUB_UNSCENTED_KALMAN_FILTER_UNSCENTED_KALMAN_FILTER_HPP

#include <Production/UnscentedKalmanFilter/UnscentedKalmanFilter.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Filters
{
   class UnscentedKalmanFilter_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_UnscentedKalmanFilter
   {
      public:
         virtual ~I_UnscentedKalmanFilter();
   };

   class _Mock_UnscentedKalmanFilter : public I_UnscentedKalmanFilter
   {
      public:
         _Mock_UnscentedKalmanFilter();
   };

   typedef ::testing::NiceMock<_Mock_UnscentedKalmanFilter> Mock_UnscentedKalmanFilter;

   void stub_setImpl(I_UnscentedKalmanFilter* pNewImpl);
}

#endif // SOURCE_TESTING_STUB_UNSCENTED_KALMAN_FILTER_UNSCENTED_KALMAN_FILTER_HPP
