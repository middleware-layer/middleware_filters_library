#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_Filters_Library                                           #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

include(ProjectConf)

###############################
### GENERAL CONFIGURATIONS  ###
###############################

if(USE_COTIRE)
    # We use cotire, simply include it
    coloredMessage(BoldBlue "using cotire" STATUS)
    include(cotire)
else()
    # We do not use cotire, create dummy function.
    coloredMessage(BoldBlue "not using cotire" STATUS)
    function(cotire)
    endfunction(cotire)
endif()

if(USE_BOOST)
    # We use Boost, simply find and include it
    set(Boost_USE_STATIC_LIBS OFF)
    set(Boost_USE_MULTITHREADED ON)
    set(Boost_USE_STATIC_RUNTIME OFF)
    find_package(Boost 1.45.0 REQUIRED)

    if(Boost_FOUND)
        include_directories(${Boost_INCLUDE_DIRS})
        add_executable(progname file1.cxx file2.cxx)
        target_link_libraries(progname ${Boost_LIBRARIES})
    endif()
endif()

if(${STATICLIB_SWITCH} STREQUAL "ON")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static -Wl,--whole-archive -lpthread -Wl,--no-whole-archive")
  set(CMAKE_FIND_LIBRARY_SUFFIXES ".a;.so")
endif()
